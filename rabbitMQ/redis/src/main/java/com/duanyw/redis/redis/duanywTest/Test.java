
package com.duanyw.redis.redis.duanywTest;

import com.duanyw.redis.redis.util.RedisUtil;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Component
public class Test {
    @Resource
    private RedisUtil redisUtil;

    Executor threadpool = Executors.newSingleThreadExecutor();

    public void  getMsg(){
        System.out.println("ceshi  huoqu DD:"+redisUtil.getItem("dd"));
    }

    private class sendMsg extends Thread{
        @Override
        public void run(){
            while(true){
                getMsg();
                try {
                    sleep(2000);
                }catch (InterruptedException e){
                    System.out.println("==========");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @PostConstruct
    public void startThred(){
        threadpool.execute(new sendMsg());
    }
}
