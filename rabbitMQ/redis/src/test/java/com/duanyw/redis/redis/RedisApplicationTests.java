package com.duanyw.redis.redis;

import com.duanyw.redis.redis.util.RedisUtil;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest
@RunWith(SpringRunner.class)
class RedisApplicationTests {
    @Resource
    private RedisUtil redisUtil;

    @Test
    void contextLoads() {
    }

    @Test
    void redisSet(){
        redisUtil.saveItem("dd","777");
        System.out.println("插入成功");
    }

    @Test
    void redisGet(){
        System.out.println(redisUtil.getItem("dd"));
    }
}
