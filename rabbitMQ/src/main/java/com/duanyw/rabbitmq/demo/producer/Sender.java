package com.duanyw.rabbitmq.demo.producer;

import com.duanyw.rabbitmq.demo.configurations.RabbitmqConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.xml.soap.Name;

/**
 * Sender
 *
 * @blame JAVA Team
 */
@Component
public class Sender {
    @Resource(name="rabbitTemplate")
    RabbitTemplate rabbitTemplate;
    @PostConstruct
    public void sendMsgByTopics() {
        /**
         * 参数：
         * 1、交换机名称
         * 2、routingKey
         * 3、消息内容
         */
        for (int i = 0; i < 5; i++) {
            String message = "恭喜您，注册成功！userid=" + i;
            rabbitTemplate.convertAndSend(RabbitmqConfig.EXCHANGE_NAME, "topic.sms.email", message);
            System.out.println(" [x] Sent '" + message + "'");
        }
    }
}
